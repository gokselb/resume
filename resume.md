  
| Last Updated |
| ----------------|
| 13.06.2019 |

- [Burak Goksel](#burak-goksel)
  - [Work Experiences](#work-experiences)
  - [Education](#education)
  - [Skills](#skills)
    - [Exam Results](#exam-results)
    - [Personal Skills](#personal-skills)
    - [Job-related Skills](#job-related-skills)
  - [Other Expriences](#other-expriences)

<p>
  <img src="image2.png">
</p>

# Burak Goksel

Software Developer.

| [linkedin](https://linkedin.com/in/burak-goksel/) | [twitter](https://twitter.com/gokseelb) | [gitlab](https://gitlab.com/gokselb) | [github](https://github.com/gokselb) | [youtube](https://www.youtube.com/channel/UCDKQ4jzY-_I3GdoHVxHbnqA) | [instagram](https://www.instagram.com/gokseelb/) | [facebook](https://www.facebook.com/gokseelb) | [Download CV](./CVBGEN.pdf) |

```json
{
  "tel": "+90 545 328 5568",
  "mail": "bgokselburak@gmail.com",
  "mail2": "burak@goksel.co",
  "birth": "25/10/1994",
  "address": "Cankaya/Ankara",
  "country": "Turkey"
}
```

## Work Experiences

```ts
- Sep 2018 - Today          Front-end Developer
                            StockMount, Ankara (Turkey)

                            - Project StockMount is a web-based integration solution to
                              sync multiple product sources with multiple marketplaces.
                            - Mainly used Angular framework for front-end management
                              interface with Agile methods.


- May 2018 - Sep 2018       Full-Stack Developer
                            Create and Soft, Ankara (Turkey)

                            - Designed, developed, and managed several websites with big
                              databases for costumers.
                            - Created web admin panels for customers to use system as a
                              combined CMS to manage their customer and content with more
                              than 5 authorization types.


- Sep 2017 - May 2018       Technical Support Phone Advisor
                            Teleperformance, Athens (Greece)

                            - Troubleshooting for Turkish-speaking costumers with Apple
                              products in the Turkish Department.
                            - Assisting customers on the phone with any questions and
                              inquiries about Apple products and services through
                              multiple channels of communication.
                            - Informing customers about features they may not be aware of.


- Jun 2017 - Sep 2017       IT Specialist
                            ESLA English Academy, Ankara (Turkey)

                            - Designed, developed, and managed websites for company.
                            - Installed and managed distant education systems.
                            - Responsible for systems maintenance.
                            - Made presentations and provided trainings for staff on use of
                              websites and in-office techs


- Feb 2017 - Jun 2017       Intern Teacher
                            Atatürk Middle School, Ministry of National Education, Ankara (Turkey)

                            - Managed the technology lab of the school.
                            - Responsible for troubleshooting.
                            - Taught computer classes for students.
                            - Provided trainings for teachers on making use of technology in their classes.
                            - Created materials and manuals for later use of the technology lab.


- Sep 2012 - Jun 2013       Intern Developer
                            MySys Technologies, Ankara (Turkey)

                            - Supported software developers.
                            - Provided developer support for internal software and applets using C#, SQL, etc.
                            - Installed systems for restaurants, such as restaurant management
                              software, and provided training for the staff on use of the systems.


```

## Education

```
Sep 2013 - Jul 2017         Bachelor of Science Degree
                            in Computer and Instructional Technologies Teacher Education
                            Faculty of Educational Sciences , Ankara University, Ankara (Turkey)

                            - Trained as an information technologies teacher. During my B.Sc. studies,
                              I learned about medium to advanced information technologies as well as acquired
                              the necessary behavioural skills to teach technology studies.
                            - Participated in technology class simulations, teaching other youth particularly
                              software and hardware skills based on their curriculum needs.
                            - Engaged in interactive working groups to develop new educational technologies
                              for the benefit of informatics teachers and students, working on
                              such diverse areas as interactive distance education tools, instructional design,
                              graphics and animation in education, etc.
                            - Conducted academic research and made several presentations.
                            - Planned, taught, and assessed classes as part of training as a teacher.


Sep 2015 - Jan 2016         Erasmus Exchange Programme in Education and Contemporary Culture
                            Faculty of Educational Sciences, University of Lower Silesia, Wrocław (Poland)

                            - Trained in contemporary educational theories and studied critical issues
                              for the future of pedagogical practices.
                            - This programme had a very intensive practical component, in which the
                              students could experience issues discussed in lectures in a concrete
                              real-life context.
                            - Participated in study visits working with other youth and learned about
                              European cultural heritage.
                            - Engaged in discussions on issues regarding adult education, teaching in a
                              multicultural environment, and civic education.


Jun 2013 - Jul 2009         Information Technologies – Database Programmer
                            Elvanköy Industrial High School, Ankara (Turkey)

                            - Trained in hardware, software, and communication technologies,
                              with a focus on database architecture.
                            - Acquired modelling and optimized query writing skills.


```

## Skills

### Exam Results

| Exam                                                                                   | Score    | Date       |
| -------------------------------------------------------------------------------------- | -------- | ---------- |
| Academic Personnel and Postgraduate Education Entrance Exam Quantitative (ALES-SAY)    | 75.65754 | 05.05.2019 |
| Academic Personnel and Postgraduate Education Entrance Exam Verbal (ALES-SÖZ)          | 48.07525 | 05.05.2019 |
| Academic Personnel and Postgraduate Education Entrance Exam Equally Weighted (ALES-EA) | 63.10618 | 05.05.2019 |
| Foreign Language Exam English (YDS - İnglizce)                                         | 70,00000 | 19.01.2019 |

### Personal Skills

<table>
  <thead>
    <tr>
      <td align=center><b>Language </td>
      <td colspan=2 align=center><b>Undertanding </td>
      <td colspan=2 align=center><b>Speaking</td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td align=center>Listening</td>
      <td align=center>Reading</td>
      <td align=center>Spoken Interaction</td>
      <td align=center>Spoken Production</td>
      <td align=center>Writing</td>
    </tr>
  </thead>
  <tbody>
     <tr>
      <td>English</td>
      <td align=center>C1</td>
      <td align=center>B2</td>
      <td align=center>C1</td>
      <td align=center>C1</td>
      <td align=center>B2</td>
    </tr>
  </tbody>
</table>

<table>
  <tbody>
     <tr>
      <td><b>Programing Languages</td>
      <td>C#, T-SQL, JavaScript, HTML, CSS, PHP, TypeScript</td>
    </tr>
     <tr>
      <td><b>Frameworks</td>
      <td>Angular 8, AngularJS, React, Laravel, MVC</td>
    </tr>
     <tr>
      <td><b>Techs</td>
      <td> Git (gitlab, github) </td>
    </tr>
  </tbody>
</table>

### Job-related Skills

```
        - Ability to work calmly under stress: In my current position, I explain to
          my customers how to overcome the problems or challenges they are experiencing,
          in a calm manner. Customers have varying degrees of
          experience with Apple products and therefore each requires assistance that
          is tailored not only to their specific needs but also their level of
          knowledge. So far, I have only received positive feedback about the
          assistance I provide and the way I provide it.
        - Problem solving abilities: All systems can fail as they are human-made. In
          a way, they are bound to fail sometimes. That is why problem-solving skills
          are highly important for any sector. I believe that whether it be an
          organizational problem or a technological failure, the following method
          should be used: identify the problem, isolate the root-cause of the problem
          (eliminate all other possible explanations), find several alternatives to
          solve the problem, if possible, and prioritize among the options. The
          positions I have held so far have enabled me to hone my problem solving
          skills greatly.
        - Ability to think and act quickly: During my bachelor education (especially during
          class simulations) as well as my work experience, I needed to make swift use of
          my analytical abilities as issues mostly arose unexpectedly.

```

## Other Expriences

```
        - Respondent for research for the thesis titled “Qualitative research on the
          use of teacher candidates in solving instructional decisions of their own
          video situations.”
        - Voluntary work for the Civil Society in the Penal System Association (CISST).
        - Worked as an IT professional and as part of the management team for the
          Project on “Prisons, Civil Society, and Universities” by CISST.
        - Dancer at Folk Dance Club of Ankara University
        - Freedom for LGBTI – Documentary about being LGBTI in Turkey (director-editor)
        – project for the Multimedia Design and Production class during
          my bachelor’s study. (https://www.youtube.com/watch?v=5YD0NAn8aZQ)

```
